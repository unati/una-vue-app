import axios from 'axios';

export const loginRequest = function (username, password, language, clientId, clientCode) {
    let urlAuthenticator = (window.location.origin.startsWith(process.env.VUE_APP_PRD_URL)?process.env.VUE_APP_PRD_AUTHENTICATOR:process.env.VUE_APP_AUTHENTICATOR);
    console.log("urlAuthenticator: "+urlAuthenticator)
    return new Promise((resolve, failure) => {
        console.log("[Authenticator] Fazendo login...");
        var data =  {
            'username': username,
            'password': password,
            'client-id': clientId,
            'language': language,
        }

        if (clientCode != null && clientCode != undefined && clientCode != '') {
            data['client-code'] = clientCode
        }

        axios({
            method: 'post',
            url: urlAuthenticator+'/login',
            data: data,
            crossDomain: true,
        }).then(response => {
            if (response.status === 202) {
                failure({ response: response });
            } else {
                const data = response.data;

                resolve({
                    ...data
                });
            }
        }).catch(response => {
            console.log("----------------");
            failure(response);
        });
    });
};


export const newCodeRequest = function (username, password, clientId, method) {
    let urlAuthenticator = (window.location.origin.startsWith(process.env.VUE_APP_PRD_URL)?process.env.VUE_APP_PRD_AUTHENTICATOR:process.env.VUE_APP_AUTHENTICATOR);
    console.log("urlAuthenticator: "+urlAuthenticator)
    return new Promise((resolve, failure) => {
        console.log("[Authenticator] Solicitando novo código...");
        axios({
            method: 'post',
            url: urlAuthenticator+'/login',
            data: {
                'username': username,
                'password': password,
                'client-id': clientId,
                'method': method
            },
            crossDomain: true,
        }).then(() => {
            resolve({});

        }).catch(response => {
            console.log("----------------");
            failure(response);
        });
    });
};


export const refreshLoginRequest = function () {
    let urlAuthenticator = (window.location.origin.startsWith(process.env.VUE_APP_PRD_URL)?process.env.VUE_APP_PRD_AUTHENTICATOR:process.env.VUE_APP_AUTHENTICATOR);
    console.log("urlAuthenticator: "+urlAuthenticator)
    console.log("[Authenticator] Renovando token...");
    var params = new URLSearchParams();
    params.append('grant_type', 'refresh_token');
    params.append('refresh_token', localStorage.getItem('refresh_token'));
    return new Promise((resolve, failure) => {
        axios({
            method: 'post',
            url: urlAuthenticator+'/access_token',
            data: params,
            crossDomain: true,
        }).then(res => {
            const data = res.data;
            resolve({
                ...data
            });
        }).catch(err => {
            failure(err);
        });
    });
};



export const userRequest = function () {
    let urlAuthenticator = (window.location.origin.startsWith(process.env.VUE_APP_PRD_URL)?process.env.VUE_APP_PRD_AUTHENTICATOR:process.env.VUE_APP_AUTHENTICATOR);
    console.log("urlAuthenticator: "+urlAuthenticator)
    console.log("[Authenticator] Buscando dados do usuário...");
    return new Promise((resolve, failure) => {
        axios({
            method: 'get',
            url: urlAuthenticator+'/me',
            crossDomain: true,
        }).then(res => {
            const data = res.data;
            resolve({
                ...data
            });
        }).catch(err => {
            failure(err);
        });
    });

};



export const updatePassword = function (resetToken, newPassword) {
    let urlAuthenticator = (window.location.origin.startsWith(process.env.VUE_APP_PRD_URL)?process.env.VUE_APP_PRD_AUTHENTICATOR:process.env.VUE_APP_AUTHENTICATOR);
    console.log("urlAuthenticator: "+urlAuthenticator)
    return new Promise((resolve, failure) => {
        axios({
            method: 'post',
            data: {
                'token': resetToken,
                'password': newPassword,
            },
            url: urlAuthenticator+'/update',
        }).then(() => {
            resolve({});
        }).catch(response => {
            failure(response);
        });
    });
};


export const resetPassword = function (usernameResetPassword, emailResetPassword, url) {
    let urlAuthenticator = (window.location.origin.startsWith(process.env.VUE_APP_PRD_URL)?process.env.VUE_APP_PRD_AUTHENTICATOR:process.env.VUE_APP_AUTHENTICATOR);
    console.log("urlAuthenticator: "+urlAuthenticator)
    return new Promise((resolve, failure) => {
        axios({
            method: 'post',
            data: {
                'username': usernameResetPassword,
                'email': emailResetPassword,
                'urlLogin': url,
            },
            url: urlAuthenticator+'/reset',
        }).then(() => {
            resolve({});
        }).catch(response => {
            failure(response);
        });
    });
};

export const resetarSenha = (username, email) => {
    let urlAuthenticator = (window.location.origin.startsWith(process.env.VUE_APP_PRD_URL)?process.env.VUE_APP_PRD_AUTHENTICATOR:process.env.VUE_APP_AUTHENTICATOR);
    console.log("urlAuthenticator: "+urlAuthenticator)
    return new Promise((resolve, failure) => {
        axios({
            method: 'post',
            data: {
                'username': username,
                'email': email,
            },
            url: urlAuthenticator+'/resetarSenha',
        }).then(() => {
            resolve({});
        }).catch(response => {
            failure(response);
        });
    });
}

export const verificaQuantidadeAcesso =  (username) => {
    let urlAuthenticator = (window.location.origin.startsWith(process.env.VUE_APP_PRD_URL)?process.env.VUE_APP_PRD_AUTHENTICATOR:process.env.VUE_APP_AUTHENTICATOR);
    console.log("urlAuthenticator: "+urlAuthenticator)
    return new Promise((resolve, failure) => {
        axios({
            method: 'get',
            url: urlAuthenticator+'/verificaQuantidadeAcesso?username='+username,
        }).then((response) => {
            resolve(response);
        }).catch(response => {
            failure(response);
        });
    });
}


