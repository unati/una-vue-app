import Vue from 'vue';
import axios from "axios";

export const Util = new Vue({
    data: {
    },
    methods: {

        processando: function() {
            this.$emit("processando");
        },

        download: function(url, nomeArquivo) {
            console.log("URL para download: "+url);
            axios({
                method: 'get',
                url: url,
                responseType: 'arraybuffer'
            }).then(response => {
                this.forceFileDownload(response, nomeArquivo)
            }).catch(() => {
                console.log('error occured')
            });
        },

        forceFileDownload: function(response, nomeArquivo) {
            console.log("Baixando arquivo: "+nomeArquivo);
            const url = window.URL.createObjectURL(new Blob([response.data]))
            const link = document.createElement('a')
            link.href = url
            link.setAttribute('download', nomeArquivo)
            document.body.appendChild(link)
            link.click()
        },

    }

});