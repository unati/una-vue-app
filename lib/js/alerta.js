import Vue from 'vue';
export const Alerta = new Vue({
    data: {

    },
    methods: {
        sucesso: function(mensagem) {
            this.$emit("alerta", mensagem, 'success');
        },
        erro: function(mensagem) {
            this.$emit("alerta", mensagem, 'error');
        },
        aviso: function(mensagem) {
            this.$emit("alerta", mensagem, 'info', 5000);
        },
    }

});