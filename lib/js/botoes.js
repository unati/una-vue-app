import Vue from 'vue';
export const Botoes = new Vue({
    data: {
        botoes: null,
        instance: null,
        editar: false,
        botaoEditar: false,
    },
    methods: {
        criaBotoesPadrao: function(i, b) {
            this.instance = i;
            this.botoes = b;
            this.$emit("botoes", this.botoes);
            this.editar = false;
        },
        criaBotoes: function (i, b) {
            this.botaoEditar = false;
            this.criaBotoesPadrao(i,b);
        },
        criaBotoesEditar: function(i, b) {
            this.botaoEditar = true;
            this.criaBotoesPadrao(i,b);
            this.instance.$root.$on("botao_editar", () => {
                this.habilitaCampos();
            });
        },
        editarCampos: function() {
            this.botaoEditar = false;
            this.$emit("botoes", this.botoes);
            this.habilitaCampos();
        },
        habilitaCampos: function() {
            this.instance.editar();
            this.editar = true;
        },
        limpa: function() {
            if (this.botoes == null) {
                return;
            }
            for (var index in Botoes.botoes) {
                this.instance.$root.$off(Botoes.botoes[index].acao);
            }
            this.instance = null;
            this.botoes = null;
        }
    }

});