import Vue from 'vue';
import axios from "axios";

export const UnaVue = new Vue({
    data: {
        botoes: null,
        instance: null,
        editar: false,
        botaoEditar: false,
        dirty: false,
        tituloTexto: null,
        navegacaoTela: null,
    },
    methods: {
        isDirty: function() {
            return this.dirty;
        },
        setDirty: function() {
            if (!this.dirty) {
                this.dirty = true;
            }
        },
        cleanDirty: function() {
            this.dirty = false;
        },


        criaBotoesPadrao: function(i, b) {
            this.instance = i;
            this.botoes = b;
            this.$emit("botoes", this.botoes);
            this.editar = false;
        },
        criaBotoes: function (i, b, consultaRapida) {
            this.botaoEditar = false;
            this.criaBotoesPadrao(i,b);
            this.botaoEditar = consultaRapida == null;
            this.editar = true
        },
        criaBotoesEditar: function(i, b) {
            this.botaoEditar = true;
            this.criaBotoesPadrao(i,b);
            this.instance.$root.$off("botao_editar");
            this.instance.$root.$on("botao_editar", () => {
                this.habilitaCampos();
            });
        },
        editarCampos: function() {
            this.botaoEditar = false;
            this.$emit("botoes", this.botoes);
            this.habilitaCampos();
        },
        habilitaCampos: function() {
            this.instance.editar();
            this.editar = true;
        },
        limpaBotoes: function() {
            if (this.botoes == null) {
                return;
            }
            for (var index in UnaVue.botoes) {
                this.instance.$root.$off(UnaVue.botoes[index].acao);
            }
            this.instance = null;
            this.botoes = null;
        },

        titulo: function(novoTitulo, novaNavegacao) {
            this.tituloTexto = novoTitulo;
            this.$emit("titulo", novoTitulo);
            if (novaNavegacao != undefined) {
                this.navegacaoTela = novaNavegacao;
                this.$emit("navegacao", novaNavegacao);
            } else {
                this.$emit("navegacao", novoTitulo);
            }
        },
        sucesso: function(mensagem) {
            this.$emit("alerta", mensagem, 'success', 5000);
        },
        erro: function(mensagem) {
            this.$emit("alerta", mensagem, 'error');
        },
        aviso: function(mensagem) {
            this.$emit("alerta", mensagem, 'info', 5000);
        },

        processando: function(loadingText) {
            this.$emit("processando", loadingText);
        },
        pararProcessando: function() {
            this.$emit("parar_processando");
        },

        download: function(url, nomeArquivo) {
            console.log("URL para download get: "+url);
            axios({
                method: 'get',
                url: url,
                responseType: 'arraybuffer'
            }).then(response => {
                this.forceFileDownload(response, nomeArquivo)
            }).catch(() => {
                console.log('Ocorreu um erro no download GET')
            });
        },

        downloadPost: function(url, nomeArquivo, postData, params) {
            console.log("URL para download post: "+url);
            axios({
                method: 'post',
                url: url,
                responseType: 'arraybuffer',
                data: postData,
                params: params
            }).then(response => {
                this.forceFileDownload(response, nomeArquivo)
            }).catch(() => {
                console.log('Ocorreu um erro no download POST')
            });
        },

        forceFileDownload: function(response, nomeArquivo) {
            console.log("Baixando arquivo: "+nomeArquivo);
            const url = window.URL.createObjectURL(new Blob([response.data]))
            const link = document.createElement('a')
            link.href = url
            link.setAttribute('download', nomeArquivo)
            document.body.appendChild(link)
            link.click()
        },

    }

});
